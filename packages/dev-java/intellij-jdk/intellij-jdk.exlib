# Copyright 2018 Alexander Kapshuna <kapsh@kap.sh>
# Distributed under the terms of the GNU General Public License v2

export_exlib_phases pkg_setup src_install

SUMMARY="JetBrains Runtime"
HOMEPAGE="https://bintray.com/jetbrains/intellij-jdk"
MY_ARCHIVE="jbsdk8u${PV/./b}_linux_x64.tar.gz"
DOWNLOADS="https://bintray.com/jetbrains/intellij-jdk/download_file?file_path=${MY_ARCHIVE} -> ${MY_ARCHIVE}"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    run:
        dev-libs/glib:2
        dev-libs/libxml2:2.0
        dev-libs/libxslt
        media-libs/freetype:2
        sys-sound/alsa-lib
        x11-libs/cairo
        x11-libs/gdk-pixbuf:2.0
        x11-libs/gtk+:2
        x11-libs/libX11
        x11-libs/libXext
        x11-libs/libXi
        x11-libs/libXrender
        x11-libs/libXtst
        x11-libs/pango
"

UPSTREAM_DOCUMENTATION="https://confluence.jetbrains.com/display/JRE/JetBrains+Runtime"

WORK="${WORKBASE}"

intellij-jdk_pkg_setup() {
    exdirectory --allow /opt
}

intellij-jdk_src_install() {
    local dest="/opt/${PN}"
    dodir "${dest}"

    insinto "${dest}"
    doins -r ./*

    edo pushd "${IMAGE}/${dest}"
    for exe in bin/* jre/bin/*; do
        edo chmod a+x "$exe"
    done
    edo popd
}

